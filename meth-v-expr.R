
# John Blischak
# 2013-07-02

# Patch to allow running on cluster or mounted
if (grepl('Ubuntu', Sys.info()['version'])) {
  PATH <- '~/spudhead'
} else {
  PATH <- ''
  .libPaths(c(.libPaths(), '/home/jdblischak/R_libs'))
}

setwd(paste0(PATH, '/mnt/lustre/data/share/Trios/methyl_data'))
suppressPackageStartupMessages(library(minfi))
library(IlluminaHumanMethylation450kannotation.ilmn.v1.2)
library(ggplot2)
suppressPackageStartupMessages(library(BSgenome))
library('BSgenome.Hsapiens.UCSC.hg19')


# Load in normalized methylation data
load('.final.data.rda') # meth.normalized

# Add annotation
anno <- c('IlluminaHumanMethylation450k', 'ilmn.v1.2')
names(anno) <- c('array', 'annotation')
annotation(meth.normalized) <- anno

# Get genomic coordinates
mapped <- mapToGenome(meth.normalized, genomeBuild = 'hg19')
# mapped is MethylGenomicSet object
# All the probes mapped successfully
dim(meth.normalized)[1] - length(mapped@rowData@ranges)

# Plot number of probes mapped by chromosome
# Also obtain chromsome lengths to standardize
chr.df <- data.frame(probe.num = mapped@rowData@seqnames@lengths,
                     chr = mapped@rowData@seqnames@values)
chr.len <- seqlengths(Hsapiens)
chr.len <- data.frame(chr = names(chr.len), len = as.numeric(chr.len))
chr.df <- merge(chr.df, chr.len, by = 'chr')
chr.df$chr <- sub('chr', '', chr.df$chr)
chr.df$chr <- ordered(chr.df$chr, levels = 1:22)
probes.by.chr <- ggplot(data = chr.df, aes(x = chr, y = probe.num))
probes.by.chr <- probes.by.chr + geom_bar(stat = 'identity') +
  labs(title = 'Distribution of probes across the autosomes',
       x = 'Autosome', y = 'Number of probes') 
ggsave('../figs/probes-by-chr.png', probes.by.chr, width = 8.5, height = 5)
# Standardize by chromosome length
probes.by.chr.std <- probes.by.chr %+% aes(y = probe.num / len)
probes.by.chr.std <- probes.by.chr.std + 
  labs(title = 'Distribution of probes across the autosomes standardized by chromosome length',
       y = 'Number of probes per chromsome bp')
ggsave('../figs/probes-by-chr-std.png', probes.by.chr.std, width = 8.5, height = 5)












