# Bryce
# Script to calculate allele-specific expression.
# Also testing first commit.

import pysam
import sys, glob, pdb

#class to store the genotype information for a trio
class SNP:
    def __init__(self, line, ccol, fcol, mcol):
        parsed_line=line.strip().split()
        position=parsed_line[0].split(".")
        #is the polymorphism a SNP or INDEL?
    
        self.pos=int(parsed_line[1])
        
        #get genotype info
        try:
            self.geno_child = parsed_line[ccol-1]
            self.geno_father = parsed_line[fcol-1]
            self.geno_mother = parsed_line[mcol-1]
        except:
            pdb.set_trace()
#get the sequences for each allele
        self.allele1 = parsed_line[4]
        self.allele2 = parsed_line[5]

        if self.is_het():
            if self.geno_father == "0":
                self.parent_allele1 = "Dad"
            else:
                self.parent_allele1 = "Mom"
        else:
            self.parent_allele1 = "NA"
            
    #is the snp homozygous in both parents and heterozygous in child?
    def is_het(self):
        return (self.geno_child == "1" and (self.geno_father=="2" and self.geno_mother=="0") or (self.geno_father=="0" and self.geno_mother=="2"))


child=sys.argv[1]
father=sys.argv[2]
mother=sys.argv[3]

childcol=0
mothercol=0
fathercol=0

outfile=open("/mnt/lustre/data/share/Trios/Data_for_analysis/AS_counts/%s_%s_%s_AS_info.bed"%(child,father,mother),"w")
key_file=open("/mnt/lustre/data/share/Trios/Genotypes/row_labels.txt")

#Which columns in the genotype file correspond to our individuals of interest?
col=1
name=key_file.readline()
while name:
    name=name.strip()
    if name==child:
        childcol=col
    if name==father:
        fathercol=col
    if name==mother:
        mothercol=col
    col+=1
    name=key_file.readline()

#Quit if we are missing one of the individuals
if(mothercol==0 or fathercol==0 or childcol==0):
    exit()

#Get the RNA-seq read files for the child
child_name=child.lstrip("NA")
lane_list=glob.glob("/mnt/lustre/data/share/Trios/Raw_Data/Children/%s/*.noXYM.bed" % (child_name))
#open a list of files containing RNA-seq reads of the child
read_files=[]
cur_reads=[]
for i in lane_list:
    lane=open(i)
    read_files.append(lane)
    cur_reads.append(lane.readline().strip().split())

#iterate through each chromosome
for chr in range(1,23): #(10,11,12,13,14,15,16,17,18,19,1,20,21,22,2,3,4,5,6,7,8,9):
    chrm="chr%i"%chr
    #open the genotype file for that chromosome
    genofile=open("/mnt/lustre/data/share/Trios/Genotypes/genotypes_%s_YRI_r27_nr.b36_fwd.hg19.genos.txt" % chrm)
    line=genofile.readline()
    #iterate through each SNP
    while line:
        #load the genotype information
        curr_snp=SNP(line,childcol,fathercol,mothercol)
        counts = [0,0]
        #is the polymorphism a heterozygous SNP?
        if curr_snp.is_het():
            #if so, then iterate through all the RNA-seq read files
            for i in range(len(read_files)):
                start=int(cur_reads[i][1])
                end=int(cur_reads[i][2])
                #throw out reads that map upstream to our current SNP
                while (cur_reads[i][0]==chrm and start<=curr_snp.pos):
                    #assign allele specificity to reads that overlap our current SNP
                    if end>curr_snp.pos:
                        diff = curr_snp.pos-start
                        #pdb.set_trace()
                        if curr_snp.allele1==cur_reads[i][3][diff-1]:
                            counts[0]+=1
                        elif curr_snp.allele2==cur_reads[i][3][diff-1]:
                            counts[1]+=1
                    cur_reads[i]=read_files[i].readline().strip().split()
                    start=int(cur_reads[i][1])
                    end=int(cur_reads[i][2])
                    #print the information for this snp
            if counts[0]+counts[1] > 0:
                outfile.write("%s\t%i\t%s\t%s\t%s\t%i\t%i\n"  % (chrm,curr_snp.pos,curr_snp.allele1,curr_snp.allele2,curr_snp.parent_allele1,counts[0],counts[1]))
                outfile.flush()
        #move on to the next SNP
        line=genofile.readline()
    #once we reach the end of a chromosome, throw out the rest of the RNA-seq reads for that chromosome
    for i in range(len(lane_list)):
        while(cur_reads[i][0]==chrm):
            cur_reads[i]=read_files[i].readline().strip().split()
    #pdb.set_trace()

