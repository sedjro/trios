#!/usr/bin/env python

'''
Calculating child genotype probabilties
=========================================

##Description

For each SNP in each child
Calculation Child genotype probability using the parent genotype probabilities.
and the Mendelian inhertiance probabilites
P(Gc = AA), P(Gc = Aa), and P(Gc = aa)
From:
P(Gpx = AA), P(Gpx = Aa), and P(Gpx = aa)
P(Gc = AA | Gp1 & Gp2), P(Gc = Aa | Gp1 & Gp2), P(Gc = aa | Gp1 & Gp2)

For that, used child_genot_prob.py script.

----------------------------------------------------

## child_genot_prob.py

Create File will contain the genotype probabilities for each child.
From parent genotype probability with the rules of Mendelian inheritance.

To run this script, It need some inputs files:

Add Inputs files like argument in command line.


- Parent genotype probability file from IMPUTE2 split by Chromosome (Frist argument)
- Parent list is Order of the individuals in IMPUTE2 (-i, --individual)
- Relationships list between individuals(-c, --children)
- Nickname of population study: Default is Yoruba "YRI" (-p, --population)
- Output file: Default stdout (-o, --output)
'''

import sys, os
import glob

def get_args(cline = sys.argv[1:]):
    '''
    '''
    import argparse
    parser = argparse.ArgumentParser(description = '''Children Genotype Probability''')
    parser.add_argument('impute_file', help = 'Parent genotype probability file from IMPUTE2')
    parser.add_argument('-i', '--individual', help = 'Parent list is Order of the individuals in IMPUTE2')
    parser.add_argument('-c', '--children', help = 'relationships list between individuals')
    parser.add_argument('-p', '--population', default = 'YRI', help = 'population study Nickname (default: Yoruba population, EX: "YRI")')    
    parser.add_argument('-o', '--output', help = 'output IMPUTE2 file [default: stdout]',
                        metavar = "fname.child.impute2")
    args = parser.parse_args(cline)

    return args

def proba_theory(par_gen, snp_id, child_id):
    '''
    Calculation of child genotype probability from parent genotype probability.
    in order : homozygous for reference allele,  heterozygous, homozygous for alternative allele
    ex:
    dad: '0.996', '0.004', '0'
    mom: '0.990', '0.010', '0'

    child: '0.993', '0.007', '0.0'
    '''
    try:
        D_RR = float(par_gen[0])
        D_RA = float(par_gen[1])
        D_AA = float(par_gen[2])
        M_RR = float(par_gen[3])
        M_RA = float(par_gen[4])
        M_AA = float(par_gen[5])    
    
        C_RR = D_RR*M_RR + D_RR*M_RA*0.5 + D_RA*M_RR*0.5 + D_RA*M_RA*0.25
        C_RA = D_RR*M_RA*0.5 + D_RR*M_AA + D_RA*M_RR*0.5 + D_RA*M_RA*0.5 + D_RA*M_AA*0.5 + D_AA*M_RR + D_AA*M_RA*0.5
        C_AA = D_RA*M_RA*0.25 + D_RA*M_AA*0.5 + D_AA*M_RA*0.5 + D_AA*M_AA

    except:

        return ['-', '-', '-']
        
    if round(C_RR + C_RA + C_AA, 3) <= 1:
        return [str(round(C_RR, 3)), str(round(C_RA, 3)), str(round(C_AA, 3))]
    elif round(C_RR + C_RA + C_AA, 3) > 1:
        return [child_id, "wrong probability"]

        
def child_line(iline, id_list, fam_list):

    '''
    SNP line with each child has three columns for their genotype.
    The order of the individuals is contained in child_sample.txt

    example for 4 children:
    
    --- rs191612142 9411243 A C 0.993 0.007 0.0 0.991 0.009 0.0 0.987 0.013 0.0 0.992 0.008 0.0
    
    '''
    
    snp_info = iline.split()[:5]
    gen_info = iline.split()[5:]

    new_line = str.join(' ', snp_info) 

    for fam in fam_list :
        D_pos = id_list[fam[1]]
        M_pos = id_list[fam[2]]
        par_gen = [gen_info[D_pos], gen_info[D_pos + 1], gen_info[D_pos + 2], gen_info[M_pos], gen_info[M_pos + 1], gen_info[M_pos + 2]]
        child_gen = proba_theory(par_gen, snp_info[1], fam[0])
        new_line = new_line + ' ' + str.join(' ', child_gen) 

    return new_line

def file_processing(impute_file, individual, fam_list, out_impute = sys.stdout):

    '''
    Create File will contain the genotype probabilities for each child and for each SNP

    input:

    Genotype data file with Parent genotype probability for each SNP split by chromosome
    The file who contains order of the individuals:
    the file with relationships between individuals
    
    '''

    sample = open(individual, 'r')
    fst_pos = 0
    id_list = {}
    while True:
        line = sample.readline()
        if not line:
            break
        ind_id = line.split()
        id_list[ind_id[0]] = fst_pos
        fst_pos += 3

    while True:
        iline = impute_file.readline()
        if not iline:
            break

        out_impute.write(child_line(iline, id_list, fam_list) + '\n')


def child_list(children, population, dir_path):
    
    """
    Get relationshipt beetween individuals in the population study with an relationship file
    Create a child list in ordered genotype
    
    """

    relation = open(children, 'r')
    fam_list = []
    while True:
        cline = relation.readline()
        if not cline:
            break
        relation_id = cline.split()
        if relation_id[-1] == population:
            if relation_id[2] != '0' and relation_id[3] != '0':
                fam_list.append([relation_id[1], relation_id[2], relation_id[3]])

    file_child_name = os.path.dirname(dir_path) + '/' + population + "_child_sample.txt"
    
    for ext_file in glob.glob(file_child_name[:-4] + '*' + file_child_name[-4:]):
        check = False
        ext = False
    
        if os.path.isfile(ext_file):
            ext = True
            check_child_file = open(ext_file, 'r')
            cnt = 0
            while True:
                tline = check_child_file.readline()
                if not tline:
                    break
                if tline.split()[0] != fam_list[cnt][0]:
                    check = True
                    break
                cnt += 1
            if cnt != len(fam_list):
                check = True
        
        check_child_file.close()
        if ext and not check:
            break
            
    if check and ext:

        nf = 1
        increase_file = file_child_name[:-4] + '_' + str(nf) + '.txt'
        while os.path.isfile(increase_file):
            nf += 1
            increase_file = file_child_name[:-4] + '_' + str(nf) + '.txt'

        
        child_file = open(increase_file, 'w')
        for chd in fam_list: 
            child_file.write(chd[0] + '\n')
            
        child_file.close()
        
    elif not ext:
    
        child_file = open(file_child_name, 'w')
        for chd in fam_list: 
            child_file.write(chd[0] + '\n')
            
        child_file.close()
            
    
    return fam_list 
    

def get_gzip(impute_file):
    
    import gzip 
    file_name = (impute_file.split('/')[-1]).split('.')[-1]
    if file_name == 'gz':
        content = gzip.open(impute_file, 'rb') 
    else:
        content = open(impute_file, 'rb')

    return content
    
#####################################################################################################
            
if __name__ == '__main__':
    args = get_args()
    impute_file = get_gzip(args.impute_file)
    if args.output:
        out_impute = open(args.out_impute, 'w')
        file_processing(impute_file, args.individual, child_list(args.children, args.population, args.out_impute),  out_impute)
    else:
        file_processing(impute_file, args.individual, child_list(args.children, args.population, args.impute_file))
