'''
Run `nosetests --verbosity=2` from base project directory.
'''

import sys
sys.path.append('scripts') 
from child_genot_prob import get_args, proba_theory, child_line, file_processing,  child_list

from nose.tools import assert_equal, assert_almost_equal, assert_true, \
    assert_false, assert_raises

from StringIO import StringIO


def test_proba_theory():

    genot = ['0.996', '0.004', '0', '0.990', '0.010', '0']
    snpID = "rs191612142"
    childID = "NA18503" 
    result = proba_theory(genot, snpID, childID)
    expected = ['0.993', '0.007', '0.0']
    assert_equal(result, expected)
    
def test_child_line():

    iline = '--- rs191612142 9411243 A C 0.996 0.004 0 0.988 0.012 0 0.994 0.006 0 0.980 0.020 0 0.996 0.004 0 0.990 0.010 0 0.992 0.008 0 0.990 \
    0.010 0 0.986 0.014 0 0.988 - 0 0.988 0.012 0 0.988 0.012 0 0.984 0.016 0 0.990 0.010 0 0.992 0.008 0 0.996 0.004 0 0.996 0.004 0 0.994 \
    0.006 0 0.994 0.006 0 0.995 0.005 0 0.998 0.002 0 0.994 0.006 0 0.988 0.012 0 0.992 0.008 0 0.994 0.006 0 0.992 0.008 0 0.988 0.012 0 0.994 \
    0.006 0 0.988 0.012 0 0.990 0.010 0 0.986 0.014 0 0.992 0.008 0 0.994 0.006 0 0.992 0.008 0 0.994 0.006 0 0.990 0.010 0 0.994 0.006 0 0.994 \
    0.006 0 0.990 0.010 0 0.990 0.010 0 0.994 0.006 0 0.988 0.012 0 0.990 0.010 0 0.988 0.012 0 0.992 0.008 0 0.990 0.010 0 0.997 0.003 0 0.990 \
    0.010 0 0.994 0.006 0 0.994 0.006 0 0.996 0.004 0 0.986 0.014 0 0.990 0.010 0 0.990 0.010 0 0.994 0.006 0 0.996 0.004 0 0.988 0.012 0 0.994 \
    0.006 0 0.992 0.008 0 0.996 0.004 0 0.992 0.008 0 0.993 0.007 0 0.993 0.007 0 0.994 0.006 0 0.984 0.016 0 0.996 0.004 0 0.994 0.006 0 0.994 \
    0.006 0 0.994 0.006 0 0.984 0.016 0 0.992 0.008 0 0.988 0.012 0 0.994 0.006 0 0.998 0.002 0 0.996 0.004 0 0.994 0.006 0 0.988 0.012 0 0.980 \
    0.020 0 0.988 0.012 0 0.991 0.009 0 0.997 0.003 0 0.996 0.004 0 0.994 0.006 0 0.993 0.007 0 0.990 0.010 0 0.982 0.018 0 0.996 0.004 0 0.990 \
    0.010 0 0.994 0.006 0 0.990 0.010 0 0.996 0.004 0 0.997 0.003 0 0.990 0.010 0 0.994 0.006 0 0.996 0.004 0 0.992 0.008 0 0.994 0.006 0 0.984 \
    0.016 0 0.994 0.006 0 0.990 0.010 0 0.994 0.006 0 0.988 0.012 0 0.996 0.004 0 0.996 0.004 0 0.996 0.004 0 0.996 0.004 0 0.990 0.010 0 0.994 \
    0.006 0 0.990 0.010 0 0.990 0.010 0 0.990 0.010 0 0.990 0.010 0 0.992 0.008 0 0.992 0.008 0 0.986 0.014 0 0.994 0.006 0 0.990 0.010 0 0.986 \
    0.014 0 0.986 0.014 0 0.992 0.008 0'

    idEX_list = {
    'NA18501':12, 'NA18502':15,
    'NA18504':18, 'NA18505':21,
    'NA18507':24, 'NA18508':27,
    'NA18859':351, 'NA18858':60}

    famEx_list = [
    ['NA18500', 'NA18501', 'NA18502'],
    ['NA18503', 'NA18504', 'NA18505'],
    ['NA18506', 'NA18507', 'NA18508'],
    ['NA18860', 'NA18859', 'NA18858']]
    
    result = child_line(iline, idEX_list, famEx_list)
    expected = '--- rs191612142 9411243 A C 0.993 0.007 0.0 0.991 0.009 0.0 - - - 0.992 0.008 0.0'
    assert_equal(result, expected)
    
def test_child_list():

    rel_file = 'tests/data/test_relationship_file.txt'
    population = 'YRI'
    dpath = 'tests/data/test_relationship_file.txt'
    child_list(rel_file, population, dpath)
    result =  open('tests/data/YRI_child_sample_2.txt', 'r')
    expected = open('tests/data/test_YRI_child_sample.txt', 'r')
    assert_equal(result.read(), expected.read())
    
def test_file_processing():
    
    result = StringIO()
    famEx_list = [
    ['NA18500', 'NA18501', 'NA18502'],
    ['NA18503', 'NA18504', 'NA18505'],
    ['NA18506', 'NA18507', 'NA18508'],
    ['NA18860', 'NA18859', 'NA18858']]
    samp_file = '/mnt/lustre/home/y.fourne/trios/data/YRI_samples.txt'
    file_processing(open('tests/data/chr21.hg19.impute2', 'r'), samp_file, famEx_list, result)
    result.seek(0)
    expected = open('tests/data/chr21.hg19.child.impute2', 'r')
    assert_equal(result.read(), expected.read())
    
