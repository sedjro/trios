
import sys, pdb


class snp():
    def __init__(self, chrom, pos, gene):
        self.chrom = chrom
        self.pos = pos
        self.gene = gene
        self.a1 = ''
        self.a2 = ''
        self.fams = {}

fname = '/data/share/Trios/Data_for_analysis/exon_AS.txt'
handle = open(fname)
colnames = handle.readline().strip().split('\t')
outfile = open('/data/share/Trios/Data_for_analysis/snp_AS.txt', 'w')
outfile.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n'%('name', 'chr', 'pos', 'ENS', 'child', 'a1', 'a2', 'a1.c', 'a2.c',
                                                                          'a1.p', 'a2.p', 'c.total', 'a1.p.total', 'a2.p.total'))
out_parent_of_origin = open('/data/share/Trios/Data_for_analysis/snp_parent_of_origin.txt', 'w')
out_parent_of_origin.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n'%('name', 'chr', 'pos', 'ENS', 'child', 'Geno_Dad', 'Geno_Mom', 'ASDad', 'ASMom', 'CountsDad', 'CountsMom', 'TotalChild', 'TotalDad', 'TotalMom'))

totals = handle.readline().strip().split('\t')
for exon in handle:
    cols = exon.strip().split('\t')
    chrom = cols[0]
    gene = cols[3]
    snp_pos = [x.split(',') for x in cols[4::7] if x != 'NA']
    # flatten the list, remove duplicates, and create list of snp instances
    snp_pos_unique = list(set([item for sublist in snp_pos for item in sublist]))
    snps = [snp(chrom, s, gene) for s in snp_pos_unique]
    i = 4
    while i < len(cols):
        fam = cols[i:i+7]
        
        fam_name = colnames[i].split('_')[0]
        if fam[0] == 'NA':
            i = i + 7
            continue
        fam_snps = fam[0].split(',')
        for j in range(len(fam_snps)):
            #if j > 0:
             #   pdb.set_trace()
            for x in snps:
                if fam_snps[j] == x.pos:
                    if x.a1 == '':
                        x.a1 = fam[1].split(',')[j]
                        x.a2 = fam[2].split(',')[j]
                    if x.a1 == fam[1].split(',')[j]:
                        # If the parental genotypes are the same
                        # as the first family with this SNP.
                        a1_child = fam[3].split(',')[j]
                        a2_child = fam[4].split(',')[j]
                        a1_parent = fam[5]
                        a2_parent = fam[6]
                        x.fams[fam_name] = [a1_child, a2_child,
                                            a1_parent, a2_parent] + totals[i+4:i+7]
                    elif x.a2 == fam[1].split(',')[j]:
                        # If the genotypes in the parents are the
                        # opposite of the first family with this SNP.
                        a1_child = fam[4].split(',')[j]
                        a2_child = fam[3].split(',')[j]
                        a1_parent = fam[6]
                        a2_parent = fam[5]
                        x.fams[fam_name] = [a1_child, a2_child,
                                            a1_parent, a2_parent] + [totals[k] for k in [i+4, i+6, i+5]]
                    else:
                        sys.stderr.write('Genotypes for %s.%s not consistent across families.\n'%(x.chrom, x.pos))
                        sys.exit(1)
                                       
                    #pdb.set_trace()
                    outfile.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n'%(x.chrom + '.' + x.pos, x.chrom, x.pos, x.gene,
                                                                                              fam_name, x.a1, x.a2, 
                                                          x.fams[fam_name][0], x.fams[fam_name][1], x.fams[fam_name][2], x.fams[fam_name][3],
                                                          x.fams[fam_name][4], x.fams[fam_name][5], x.fams[fam_name][6]))
                    # Get information on maternal v. paternal inheritance of expression
                    out_parent_of_origin.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n'%(x.chrom + '.' + x.pos, x.chrom, x.pos,
                                                                                                           x.gene,
                                                                                               fam_name, fam[1].split(',')[j], fam[2].split(',')[j],
                                                                                               fam[3].split(',')[j], fam[4].split(',')[j],
                                                                                               fam[5], fam[6], totals[i+4], totals[i+5], totals[i+6])) 
        i = i + 7
 
handle.close()
outfile.close()
out_parent_of_origin.close()
